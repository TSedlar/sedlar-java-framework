package me.sedlar.util.collections;

import me.sedlar.util.Filter;

import java.util.Collections;
import java.util.LinkedList;

/**
 * @author Tyler Sedlar
 */
public class QueryableList<T> extends LinkedList<T> {

    /**
     * Gets the first value that matches the given filter.
     *
     * @param filter The filter to match a value against.
     * @return The first value that matches the given filter.
     */
    public T first(Filter<T> filter) {
        for (T t : this) {
            if (filter.accept(t))
                return t;
        }
        return null;
    }

    /**
     * Gets all values that match the given filter.
     *
     * @param filter The filter to match values against.
     * @return All values that match the given filter.
     */
    public QueryableList<T> all(Filter<T> filter) {
        QueryableList<T> list = new QueryableList<>();
        for (T t : this) {
            if (filter.accept(t))
                list.add(t);
        }
        return list;
    }

    /**
     * Gets the first value that matches the given filter after the given index.
     *
     * @param index The index to match after.
     * @param filter The filter to match a value against.
     * @return The first value that matches the given filter after the given index.
     */
    public T firstAfter(int index, Filter<T> filter) {
        for (int i = index; i < size(); i++) {
            T t = get(i);
            if (filter.accept(t))
                return t;
        }
        return null;
    }

    /**
     * Gets the first value that matches the given filter before the given index.
     *
     * @param index The index to match before.
     * @param filter The filter to match a value against.
     * @return The first value that matches the given filter before the given index.
     */
    public T firstBefore(int index, Filter<T> filter) {
        for (int i = 0; i < index; i--) {
            T t = get(i);
            if (filter.accept(t))
                return t;
        }
        return null;
    }

    /**
     * Gets all values that match the given filter after the given index.
     *
     * @param index The index to match after.
     * @param filter The filter to match a value against.
     * @return All values that match the given filter after the given index.
     */
    public QueryableList<T> allAfter(int index, Filter<T> filter) {
        QueryableList<T> list = new QueryableList<>();
        for (int i = index; i < size(); i++) {
            T t = get(i);
            if (filter.accept(t))
                list.add(t);
        }
        return list;
    }

    /**
     * Gets all values that match the given filter before the given index.
     *
     * @param index The index to match before.
     * @param filter The filter to match a value against.
     * @return All values that match the given filter before the given index.
     */
    public QueryableList<T> allBefore(int index, Filter<T> filter) {
        QueryableList<T> list = new QueryableList<>();
        for (int i = 0; i < index; i++) {
            T t = get(i);
            if (filter.accept(t))
                list.add(t);
        }
        return list;
    }

    /**
     * Gets the nearest value that matches the given filter.
     *
     * @param index The index to match near.
     * @param filter The filter to match a value against.
     * @return The nearest value that matches the given filter.
     */
    public T nearest(int index, Filter<T> filter) {
        for (int i = 0; i < index / 2; i++) {
            if (index - i >= 0) {
                T a = get(index - i);
                if (filter.accept(a))
                    return a;
            }
            if (index + i < size()) {
                T b = get(index + i);
                if (filter.accept(b))
                    return b;
            }
        }
        return null;
    }

    /**
     * Shuffles the order of this list
     */
    public void shuffle() {
        Collections.shuffle(this);
    }

    /**
     * Gets a random value out of all the values that match the given filter.
     *
     * @param filter The filter to match a value against.
     * @return A random value out of all of the values that match the given filter.
     */
    public T random(Filter<T> filter) {
        return all(filter).random();
    }

    /**
     * Gets a random value from this list.
     *
     * @return A random value from this list.
     */
    public T random() {
        QueryableList<T> list = new QueryableList<>();
        list.addAll(this);
        list.shuffle();
        return list.get((int) (Math.random() * (list.size() - 1)));
    }
}
