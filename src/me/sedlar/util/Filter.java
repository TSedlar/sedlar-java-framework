package me.sedlar.util;

/**
 * @author Tyler Sedlar
 */
public interface Filter<T> {

    /**
     * Tells whether or not the passed argument is accepted.
     *
     * @param t The value to check for acception.
     * @return <t>true</t> if the argument was accepted, otherwise <t>false</t>.
     */
    public boolean accept(T t);
}
